bootstrap-bundle
================
JhgBootstrapBundle helps <a href="http://getbootstrap.com/">Bootstrap</a> integration in Symfony2 projects.


Instalation
===========
```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/javihernandezgil/bootstrap-bundle.git"
        }
    ],
    "require": {
        "javihernandezgil/bootstrap-bundle": "dev-master"
    }
}
```

Boostrap changes
================
Following the instructions given in [https://github.com/leafo/lessphp/issues/491#issuecomment-28142698](https://github.com/leafo/lessphp/issues/491#issuecomment-28142698), some changes has been added to fix the leafo/lessphp compiling error.